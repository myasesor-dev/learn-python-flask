from crypt import methods
from ensurepip import bootstrap
from multiprocessing import context
from flask import request, make_response, redirect, render_template, session, url_for, flash;
import unittest
import logging # es como el logger
from app import create_app # como esta funcion esta en el __init__.py se pueden importar asi
from app.forms import LoginForm

#app = Flask( __name__)
#bootstrap = Bootstrap(app)
#app.config["SECRET_KEY"]='UNA CLAVE MUY SEGURA'
app = create_app()

items = ["Uva", "Pina", "Mango", "Patilla"]

@app.route("/hola")
def hello():
    ipdelusuario=request.remote_addr
    username=request.remote_user
    return f"Hola tu IP Es -> {ipdelusuario} y tu nombre es {username}"


@app.cli.command()
def test():
    tests = unittest.TestLoader().discover("tests")
    unittest.TextTestRunner().run(tests) # tests es la carpeta donde se guardan todos los tests

@app.errorhandler(404)
def not_found_endpoint(error):
    return render_template('404.html', error=error)

@app.route("/index")
def index():
    user_ip_information=request.remote_addr
    response = make_response(redirect("/show_information_address"))
    #responsex.set_cookie("infoIp", ipdelusuario)
    session["user_ip_information"] = user_ip_information

    return response

@app.route("/show_information_address")
def show_information():
    #userIp=request.cookies.get("infoIp")
    user_ip=session.get("user_ip_information")
    username = session.get("username")
    

    context = {
        "user_ip": user_ip,
        "items": items,
        "username": username
    }
    logging.error("imprimiendo un log................")



    #return f"Hola ti Ip es : {userIp}"
    #return render_template("ip_information.html", userIpX=userIp)
    #return render_template("ip_information.html", varloquesea=context); Esto es uma forma de enviar como objeto
    return render_template("ip_information.html", **context) # asi lo mando como referencia y acada variable se usa sin context.userIp

if __name__== "__main__":
    app.run(host='0.0.0.0', port=81, debug=True)
