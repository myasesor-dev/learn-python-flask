from flask_testing import TestCase
from flask import current_app, url_for
from werkzeug.wrappers import response
from main import app

class MaintTest(TestCase):
    def create_app(self):
        app.config["TESTING"] = True # Esto le dice a flask que simule un ambiente de prueba ya no es development ni producction
        app.config["WTF_CSRF_ENABLED"] = False # Esto lo que hace es deshabilitar el token en ambiente test
        return app

    def test_app_exists_web(self):
        self.assertIsNotNone(current_app) # Verifica que un objeto no sea del tipo NoN y referencia nuestea APP creada en el ambiente test

    def test_app_testing_mode(self):
        self.assertTrue(current_app.config["TESTING"]) # assertTrue verifica que el parametro TESTING sea True
    
    def test_index_redirects(self):
        response = self.client.get(url_for("index"))
        self.assertRedirects(response, url_for("show_information"))
    

    def test_show_information_get(self):
        response = self.client.get(url_for("show_information"))
        self.assert200(response)
    
    def test_show_information_post(self):
        response = self.client.post(url_for("show_information"))
        self.assert405(response )

    def test_auth_blueprint_exists_module(self):
        self.assertIn("auth", self.app.blueprints) # comprueba que aut este en la lista de blueprints creados

    def test_auth_blueprint_login_get(self):
        response = self.client.get(url_for("auth.login"))
        self.assert200(response)

    def test_auth_bluetprint_login_template(self):
        self.client.get(url_for("auth.login"))
        self.assertTemplateUsed("login.html")

    def test_auth_blueprint_login_post(self):
        test_form_fake = {
            "username": "RogerJose",
            "password": "rjrc*072"
        }
        response = self.client.post(url_for("auth.login"), data = test_form_fake)
        self.assertRedirects(response, url_for("index"))

